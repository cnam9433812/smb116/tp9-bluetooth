package francois.rabanel.tp9;

public class Device {
    String TAG = "DeviceClass";
    private String name;
    private String mac;
    public Device(String deviceName, String deviceHardwareAddress) {
        this.name = deviceName;
        this.mac = deviceHardwareAddress;
    }

    public String getName() {
        return this.name;
    }
    public String getMAC() {
        return this.mac;
    }
}
