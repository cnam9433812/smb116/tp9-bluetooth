package francois.rabanel.tp9.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import francois.rabanel.tp9.R;

public class DeviceAdapter extends BaseAdapter {
    String TAG = "DeviceAdapter";
    private String name;
    private String mac;
    public DeviceAdapter(Context applicationContext, int device_item, String deviceName, String deviceHardwareAddress) {
        this.name = deviceName;
        this.mac = deviceHardwareAddress;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView , ViewGroup parent) {
        final View result;

        if (convertView == null) {
            result = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_item, parent, false);
        } else {
            result = convertView;
        }
        ((TextView) result.findViewById(R.id.device_mac_txt)).setText(this.mac);
        ((TextView) result.findViewById(R.id.device_name_txt)).setText(this.name);
        return result;
    }
}
