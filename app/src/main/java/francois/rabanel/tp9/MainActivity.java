package francois.rabanel.tp9;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;

import francois.rabanel.tp9.adapters.DeviceAdapter;

public class MainActivity extends AppCompatActivity {
    public static String TAG = "MainActivity";
    BluetoothManager bluetoothManager;
    BluetoothAdapter bluetoothAdapter;
    Integer REQUEST_ENABLE_BT = 0;
    Button discoverButton;
    ListView listDevicesView;
    TextView bluetoothAdapterNameTxt, bluetoothAdapterAddressTxt, bluetoothAdapterStateTxt, bluetoothAdapterScanTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.BLUETOOTH_CONNECT}, 10);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.BLUETOOTH_SCAN}, 100);
        }

        listDevicesView = (ListView) findViewById(R.id.list_devices);

        bluetoothAdapterNameTxt = (TextView) findViewById(R.id.blt_adapter_name_txt);
        bluetoothAdapterAddressTxt = (TextView) findViewById(R.id.blt_adapter_address_txt);
        bluetoothAdapterStateTxt = (TextView) findViewById(R.id.blt_state_txt);
        bluetoothAdapterScanTxt = (TextView) findViewById(R.id.blt_scan_mode_txt);

        bluetoothManager = getSystemService(BluetoothManager.class);
        bluetoothAdapter = bluetoothManager.getAdapter();

        bluetoothAdapterNameTxt.setText("BLUETOOTH ADAPTER NAME: " + bluetoothAdapter.getName());
        bluetoothAdapterAddressTxt.setText("BLUETOOTH ADAPTER ADDRESS: " + bluetoothAdapter.getAddress());
        bluetoothAdapterStateTxt.setText("STATE: " + isStateON(bluetoothAdapter));
        bluetoothAdapterScanTxt.setText("SCAN MODE: " + isScanConnectable(bluetoothAdapter));

    }

    @Override
    protected void onResume() {
        super.onResume();
        discoverButton = (Button) findViewById(R.id.discover_button);
        discoverButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View view) {
                getAllreadyPairedDevices();

                // Lets go discovery
                IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                registerReceiver(receiver, filter);

                bluetoothAdapter.startDiscovery();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                @SuppressLint("MissingPermission") String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address

                Log.d(TAG, "Found device Name: " + deviceName);
                Log.d(TAG, "Found MAC Address: " + deviceHardwareAddress);
            }
        }
    };


    @SuppressLint("MissingPermission")
    private String isScanConnectable(BluetoothAdapter adapter) {
        if (adapter.getScanMode() == adapter.SCAN_MODE_CONNECTABLE) {
            return new String("CONNECTABLE");
        } else {
            return new String("OFF");
        }
    }

    private String isStateON(BluetoothAdapter adapter) {
        if (adapter.getState() == adapter.STATE_ON) {
            return new String("ON");
        } else {
            return new String("OFF");
        }
    }

    private void getAllreadyPairedDevices() {
        @SuppressLint("MissingPermission") Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                @SuppressLint("MissingPermission") String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                Log.d(TAG, "paired Device name: " + deviceName);
                Log.d(TAG, "paired Device MAC: " + deviceHardwareAddress);
            }
        }
    }
}